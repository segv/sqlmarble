# NOTE See https://docs.pytest.org/en/7.1.x/example/nonpython.html 20220620:mb
import pytest


def pytest_collect_file(parent, file_path):
    if file_path.suffix == ".yaml" and file_path.name.startswith("test_"):
        return YamlFile.from_parent(parent, path=file_path)


class YamlFile(pytest.File):
    def collect(self):
        import yaml

        raw = yaml.safe_load(self.path.open())
        dialect = raw.pop("dialect", None)
        max_line_length = raw.pop("max_line_length", None)

        for name, spec in sorted(raw.items()):
            yield YamlItem.from_parent(
                self,
                name=name,
                spec=spec,
                dialect=dialect,
                max_line_length=max_line_length,
            )


class YamlItem(pytest.Item):
    def __init__(self, *, spec, dialect=None, max_line_length=None, **kwargs):
        super().__init__(**kwargs)
        self.dialect = dialect
        self.max_line_length = max_line_length
        self.spec = spec

    def runtest(self):
        give = self.spec["give"]
        want = self.spec["want"]
        want = want.replace("\\n", "\n")
        want = want.replace("\\t", "\t")
        want = want.replace("\\\\", "\\")

        from sqlmarble.formatter import format_sql_string

        formatted = format_sql_string(
            give,
            dialect=self.spec.get("dialect", self.dialect),
            max_line_length=self.spec.get("max_line_length", self.max_line_length),
        )
        assert formatted.rstrip() == want.rstrip()

    def reportinfo(self):
        return self.path, 0, f"usecase: {self.name}"
