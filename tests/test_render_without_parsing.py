from sqlmarble.formatter import render_to_chunks
from sqlmarble.nodes import CommaList, Text, Clause, Indent, Sequence, Newline


def seq(*args):
    return Sequence([Text(arg) if isinstance(arg, str) else arg for arg in args])


def nl(*args):
    return Newline()


def test_Text():
    assert render_to_chunks(Text("foo")) == "foo"


def test_Sequence_1():
    assert render_to_chunks(seq("foo")) == "foo"


def test_Sequence_2():
    assert render_to_chunks(seq("foo", nl())) == "foo\n"


def test_Sequence_3():
    assert render_to_chunks(seq("foo", "bar")) == "foobar"


def test_Sequence_4():
    assert render_to_chunks(seq("foo", nl(), "bar")) == "foo\nbar"


def test_Indent_1():
    node = seq("select ", Indent(seq(nl(), "1", nl(), "2")))
    assert render_to_chunks(node) == "select \n  1\n  2"


def test_Indent_2():
    node = seq(
        "select ",
        Indent(seq(nl(), "1", nl(), "2")),
        nl(),
        "from ",
        Indent(seq(nl(), "table")),
    )
    assert render_to_chunks(node) == "select \n  1\n  2\nfrom \n  table"


def test_Indent_3():
    node = seq(
        "a1", Indent(seq(nl(), "b1", Indent(seq(nl(), "c")), nl(), "b2")), nl(), "a2"
    )
    assert render_to_chunks(node) == "a1\n  b1\n    c\n  b2\na2"


def test_CommaList_1():
    node = CommaList([seq("1"), seq("2"), seq("3")])
    assert render_to_chunks(node, max_line_length=0) == "1\n,2\n,3"


def test_CommaList_2():
    node = CommaList([seq("1"), seq("2"), seq("3")])
    assert render_to_chunks(node, max_line_length=999) == "1, 2, 3"


def test_Clause_1():
    node = Clause(Text("select"), Text("expression"))
    assert render_to_chunks(node) == "select expression"


def test_Clause_2():
    node = Clause(Text("select"), Text("expression"))
    assert render_to_chunks(node, max_line_length=0) == "select\n  expression"


def test_Clause_3a():
    node = Clause(Text("select"), CommaList([seq("1"), seq("2"), seq("3")]))
    assert (
        render_to_chunks(node, max_line_length=len("select 1, 2, 3"))
        == "select 1, 2, 3"
    )


def test_Clause_3b():
    node = Clause(Text("select"), CommaList([seq("1"), seq("2"), seq("3")]))
    assert (
        render_to_chunks(node, max_line_length=len("select 1, 2, 3") - 1)
        == "select\n  1, 2, 3"
    )


def test_Clause_3c():
    node = Clause(Text("select"), CommaList([seq("1"), seq("2"), seq("3")]))
    assert render_to_chunks(node, max_line_length=0) == "select\n  1\n  ,2\n  ,3"


def test_Clause_3d():
    node = Clause(Text("select"), CommaList([seq("1"), seq("1234567890")]))
    assert (
        render_to_chunks(node, max_line_length=len("select"))
        == "select\n  1\n  ,1234567890"
    )
