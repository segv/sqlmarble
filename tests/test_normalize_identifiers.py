from sqlmarble.dialects_bigquery import BigQuery
from sqlmarble.dialects_postgres import Postgres


def test_bq_normalize():
    bq = BigQuery()
    assert bq.normalize_identifier("`project`") == "project"
    assert bq.normalize_identifier("`dataset`") == "dataset"
    assert bq.normalize_identifier("`table`") == "`table`"
    assert (
        bq.normalize_identifier("`project.dataset.table`") == "project.dataset.`table`"
    )
    assert bq.normalize_identifier("`and`") == "`and`"
    assert bq.normalize_identifier("`PROJECT`") == "`PROJECT`"
    assert bq.normalize_identifier("foo_bar") == "foo_bar"
    assert bq.normalize_identifier("`1`") == "`1`"
    assert bq.normalize_identifier("`table_name`") == "table_name"


def test_pg_normalize():
    pg = Postgres()
    assert pg.normalize_identifier("project") == "project"
    assert pg.normalize_identifier("t") == "t"
    assert pg.normalize_identifier("select") == '"select"'
    assert pg.normalize_identifier('"and"') == '"and"'
    assert pg.normalize_identifier("TABLE$") == "table$"
    assert pg.normalize_identifier('"FOO_bar"') == '"FOO_bar"'
    assert pg.normalize_identifier('"1"') == '"1"'
    assert pg.normalize_identifier('"TABLE_NAME"') == '"TABLE_NAME"'
    assert pg.normalize_identifier("TABLE_NAME") == "table_name"
