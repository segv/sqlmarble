import sqlfluff


def test_simple_whitespace_1():
    assert sqlfluff.parse("select 1") == {
        "file": {
            "statement": {
                "select_statement": {
                    "select_clause": {
                        "keyword": "select",
                        "select_clause_element": {"literal": "1"},
                        "whitespace": " ",
                    }
                }
            }
        }
    }


def test_simple_whitespace_2():
    assert sqlfluff.parse("select  1") == {
        "file": {
            "statement": {
                "select_statement": {
                    "select_clause": {
                        "keyword": "select",
                        "select_clause_element": {"literal": "1"},
                        "whitespace": "  ",
                    },
                },
            },
        }
    }


def test_simple_whitespace_3():
    assert sqlfluff.parse("select  1  ") == {
        "file": {
            "statement": {
                "select_statement": {
                    "select_clause": {
                        "keyword": "select",
                        "select_clause_element": {"literal": "1"},
                        "whitespace": "  ",
                    },
                },
            },
            "whitespace": "  ",
        }
    }


def test_simple_whitespace_4():
    assert sqlfluff.parse("  select  1  ") == {
        "file": [
            {"whitespace": "  "},
            {
                "statement": {
                    "select_statement": {
                        "select_clause": {
                            "keyword": "select",
                            "select_clause_element": {"literal": "1"},
                            "whitespace": "  ",
                        },
                    },
                }
            },
            {"whitespace": "  "},
        ],
    }


def test_simple_function_call():
    assert sqlfluff.parse("select sum  (   2    )") == {
        "file": {
            "statement": {
                "select_statement": {
                    "select_clause": {
                        "keyword": "select",
                        "select_clause_element": {
                            "function": {
                                "bracketed": [
                                    {"start_bracket": "("},
                                    {"whitespace": "   "},
                                    {"expression": {"literal": "2"}},
                                    {"whitespace": "    "},
                                    {"end_bracket": ")"},
                                ],
                                "function_name": {"function_name_identifier": "sum"},
                                "whitespace": "  ",
                            },
                        },
                        "whitespace": " ",
                    },
                }
            },
        }
    }


def test_simple_from():
    assert sqlfluff.parse("select  1  from a") == {
        "file": {
            "statement": {
                "select_statement": {
                    "select_clause": {
                        "keyword": "select",
                        "select_clause_element": {"literal": "1"},
                        "whitespace": "  ",
                    },
                    "from_clause": {
                        "from_expression": {
                            "from_expression_element": {
                                "table_expression": {
                                    "table_reference": {"identifier": "a"}
                                }
                            }
                        },
                        "keyword": "from",
                        "whitespace": " ",
                    },
                    "whitespace": "  ",
                }
            },
        }
    }


def test_coment_only():
    assert sqlfluff.parse(" ") == {"file": {"whitespace": " "}}
    assert sqlfluff.parse("-- hi") == {"file": {"comment": "-- hi"}}
    assert sqlfluff.parse("-- hi\n-- bye") == {
        "file": [{"comment": "-- hi"}, {"newline": "\n"}, {"comment": "-- bye"}]
    }


def test_select_with_from():
    assert sqlfluff.parse("select * from t")["file"]["statement"] == {
        "select_statement": {
            "from_clause": {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "t"}}
                    }
                },
                "keyword": "from",
                "whitespace": " ",
            },
            "select_clause": {
                "keyword": "select",
                "select_clause_element": {
                    "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
                },
                "whitespace": " ",
            },
            "whitespace": " ",
        }
    }


def test_select_with_from_two_tables():
    assert sqlfluff.parse("select * from t1, t2")["file"]["statement"] == {
        "select_statement": {
            "from_clause": [
                {"keyword": "from"},
                {"whitespace": " "},
                {
                    "from_expression": {
                        "from_expression_element": {
                            "table_expression": {
                                "table_reference": {"identifier": "t1"}
                            }
                        }
                    }
                },
                {"comma": ","},
                {"whitespace": " "},
                {
                    "from_expression": {
                        "from_expression_element": {
                            "table_expression": {
                                "table_reference": {"identifier": "t2"}
                            }
                        }
                    }
                },
            ],
            "select_clause": {
                "keyword": "select",
                "select_clause_element": {
                    "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
                },
                "whitespace": " ",
            },
            "whitespace": " ",
        }
    }


def test_select_escaped_identifier():
    parsed = sqlfluff.parse('select "t"."foo" as "t.foo"', dialect="postgres")
    statement = parsed["file"]["statement"]
    assert statement == {
        "select_statement": {
            "select_clause": {
                "keyword": "select",
                "select_clause_element": {
                    "alias_expression": {
                        "identifier": '"t.foo"',
                        "keyword": "as",
                        "whitespace": " ",
                    },
                    "column_reference": [
                        {"identifier": '"t"'},
                        {"dot": "."},
                        {"identifier": '"foo"'},
                    ],
                    "whitespace": " ",
                },
                "whitespace": " ",
            }
        }
    }


def test_select_lots_of_spaces_no_trailing_space():
    parsed = sqlfluff.parse(
        "SELECT         1     \n\n\n\n\n  FROM      \n\n\n\n\n t1       , \t\t\t\t\t\n\t\t\t t2"
    )
    assert parsed == {
        "file": {
            "statement": {
                "select_statement": [
                    {
                        "select_clause": {
                            "keyword": "SELECT",
                            "select_clause_element": {"literal": "1"},
                            "whitespace": "         ",
                        }
                    },
                    {"whitespace": "     "},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"whitespace": "  "},
                    {
                        "from_clause": [
                            {"keyword": "FROM"},
                            {"whitespace": "      "},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"whitespace": " "},
                            {
                                "from_expression": {
                                    "from_expression_element": {
                                        "table_expression": {
                                            "table_reference": {"identifier": "t1"}
                                        }
                                    },
                                    "whitespace": "       ",
                                }
                            },
                            {"comma": ","},
                            {"whitespace": " \t\t\t\t\t"},
                            {"newline": "\n"},
                            {"whitespace": "\t\t\t "},
                            {
                                "from_expression": {
                                    "from_expression_element": {
                                        "table_expression": {
                                            "table_reference": {"identifier": "t2"}
                                        }
                                    }
                                }
                            },
                        ]
                    },
                ]
            }
        }
    }


def test_select_lots_of_spaces_and_trailing_space():
    parsed = sqlfluff.parse(
        "SELECT         1     \n\n\n\n\n  FROM      \n\n\n\n\n t1       , \t\t\t\t\t\n\t\t\t t2\n    \n\n\n\n\n     "
    )
    assert parsed == {
        "file": [
            {
                "statement": {
                    "select_statement": [
                        {
                            "select_clause": {
                                "keyword": "SELECT",
                                "select_clause_element": {"literal": "1"},
                                "whitespace": "         ",
                            }
                        },
                        {"whitespace": "     "},
                        {"newline": "\n"},
                        {"newline": "\n"},
                        {"newline": "\n"},
                        {"newline": "\n"},
                        {"newline": "\n"},
                        {"whitespace": "  "},
                        {
                            "from_clause": [
                                {"keyword": "FROM"},
                                {"whitespace": "      "},
                                {"newline": "\n"},
                                {"newline": "\n"},
                                {"newline": "\n"},
                                {"newline": "\n"},
                                {"newline": "\n"},
                                {"whitespace": " "},
                                {
                                    "from_expression": {
                                        "from_expression_element": {
                                            "table_expression": {
                                                "table_reference": {"identifier": "t1"}
                                            }
                                        },
                                        "whitespace": "       ",
                                    }
                                },
                                {"comma": ","},
                                {"whitespace": " \t\t\t\t\t"},
                                {"newline": "\n"},
                                {"whitespace": "\t\t\t "},
                                {
                                    "from_expression": {
                                        "from_expression_element": {
                                            "table_expression": {
                                                "table_reference": {"identifier": "t2"}
                                            }
                                        }
                                    }
                                },
                            ]
                        },
                    ]
                }
            },
            {"newline": "\n"},
            {"whitespace": "    "},
            {"newline": "\n"},
            {"newline": "\n"},
            {"newline": "\n"},
            {"newline": "\n"},
            {"newline": "\n"},
            {"whitespace": "     "},
        ]
    }


def test_select_lots_of_spaces_no_trailing_space():
    parsed = sqlfluff.parse(
        "SELECT         1     \n\n\n\n\n  FROM      \n\n\n\n\n t1       , \t\t\t\t\t\n\t\t\t t2"
    )
    assert parsed == {
        "file": {
            "statement": {
                "select_statement": [
                    {
                        "select_clause": {
                            "keyword": "SELECT",
                            "select_clause_element": {"literal": "1"},
                            "whitespace": "         ",
                        }
                    },
                    {"whitespace": "     "},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"newline": "\n"},
                    {"whitespace": "  "},
                    {
                        "from_clause": [
                            {"keyword": "FROM"},
                            {"whitespace": "      "},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"newline": "\n"},
                            {"whitespace": " "},
                            {
                                "from_expression": {
                                    "from_expression_element": {
                                        "table_expression": {
                                            "table_reference": {"identifier": "t1"}
                                        }
                                    },
                                    "whitespace": "       ",
                                }
                            },
                            {"comma": ","},
                            {"whitespace": " \t\t\t\t\t"},
                            {"newline": "\n"},
                            {"whitespace": "\t\t\t "},
                            {
                                "from_expression": {
                                    "from_expression_element": {
                                        "table_expression": {
                                            "table_reference": {"identifier": "t2"}
                                        }
                                    }
                                }
                            },
                        ]
                    },
                ]
            }
        }
    }


def test_select_biquery_column_reference():
    parsed = sqlfluff.parse(
        """SELECT "a", 'a', `a`, `a`.`b`, `a.b`""",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"]["select_clause"] == [
        {"keyword": "SELECT"},
        {"whitespace": " "},
        {"select_clause_element": {"literal": '"a"'}},
        {"comma": ","},
        {"whitespace": " "},
        {"select_clause_element": {"literal": "'a'"}},
        {"comma": ","},
        {"whitespace": " "},
        {"select_clause_element": {"column_reference": {"identifier": "`a`"}}},
        {"comma": ","},
        {"whitespace": " "},
        {
            "select_clause_element": {
                "column_reference": [
                    {"identifier": "`a`"},
                    {"dot": "."},
                    {"identifier": "`b`"},
                ]
            }
        },
        {"comma": ","},
        {"whitespace": " "},
        {"select_clause_element": {"column_reference": {"identifier": "`a.b`"}}},
    ]


def test_select_biquery_table_reference_in_pieces():
    parsed = sqlfluff.parse(
        """SELECT `table`.`column` FROM `project`.`dataset`.`table`""",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": {
                "from_expression_element": {
                    "table_expression": {
                        "table_reference": [
                            {"identifier": "`project`"},
                            {"dot": "."},
                            {"identifier": "`dataset`"},
                            {"dot": "."},
                            {"identifier": "`table`"},
                        ]
                    }
                }
            },
            "keyword": "FROM",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "SELECT",
            "select_clause_element": {
                "column_reference": [
                    {"identifier": "`table`"},
                    {"dot": "."},
                    {"identifier": "`column`"},
                ]
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_select_biquery_table_reference_joined():
    parsed = sqlfluff.parse(
        """SELECT 1 FROM `project.dataset.table`""",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": {
                "from_expression_element": {
                    "table_expression": {
                        "table_reference": {"identifier": "`project.dataset.table`"}
                    }
                }
            },
            "keyword": "FROM",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "SELECT",
            "select_clause_element": {"literal": "1"},
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_comma_list():
    parsed = sqlfluff.parse(
        """select * from a, b""",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": [
            {"keyword": "from"},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "a"}}
                    }
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "b"}}
                    }
                }
            },
        ],
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_with_alias():
    parsed = sqlfluff.parse(
        'select * from a_table as "Select"',
        dialect="postgres",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": {
                "from_expression_element": {
                    "alias_expression": {
                        "identifier": '"Select"',
                        "keyword": "as",
                        "whitespace": " ",
                    },
                    "table_expression": {"table_reference": {"identifier": "a_table"}},
                    "whitespace": " ",
                }
            },
            "keyword": "from",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_join():
    parsed = sqlfluff.parse(
        "select * from a cross join b using (id)",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": {
                "from_expression_element": {
                    "table_expression": {"table_reference": {"identifier": "a"}}
                },
                "join_clause": [
                    {"keyword": "cross"},
                    {"whitespace": " "},
                    {"keyword": "join"},
                    {"whitespace": " "},
                    {
                        "from_expression_element": {
                            "table_expression": {"table_reference": {"identifier": "b"}}
                        }
                    },
                    {"whitespace": " "},
                    {"keyword": "using"},
                    {"whitespace": " "},
                    {
                        "bracketed": {
                            "end_bracket": ")",
                            "identifier": "id",
                            "start_bracket": "(",
                        }
                    },
                ],
                "whitespace": " ",
            },
            "keyword": "from",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_join_with_alias():
    parsed = sqlfluff.parse(
        "select * from a_table as A inner join B as `select` using (id)",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": {
                "from_expression_element": {
                    "alias_expression": {
                        "identifier": "A",
                        "keyword": "as",
                        "whitespace": " ",
                    },
                    "table_expression": {"table_reference": {"identifier": "a_table"}},
                    "whitespace": " ",
                },
                "join_clause": [
                    {"keyword": "inner"},
                    {"whitespace": " "},
                    {"keyword": "join"},
                    {"whitespace": " "},
                    {
                        "from_expression_element": {
                            "alias_expression": {
                                "identifier": "`select`",
                                "keyword": "as",
                                "whitespace": " ",
                            },
                            "table_expression": {
                                "table_reference": {"identifier": "B"}
                            },
                            "whitespace": " ",
                        }
                    },
                    {"whitespace": " "},
                    {"keyword": "using"},
                    {"whitespace": " "},
                    {
                        "bracketed": {
                            "end_bracket": ")",
                            "identifier": "id",
                            "start_bracket": "(",
                        }
                    },
                ],
                "whitespace": " ",
            },
            "keyword": "from",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_join_cross_and_comma_tables():
    parsed = sqlfluff.parse(
        "select * from a, b, c cross join d, e",
        dialect="bigquery",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": [
            {"keyword": "from"},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "a"}}
                    }
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "b"}}
                    }
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "c"}}
                    },
                    "join_clause": [
                        {"keyword": "cross"},
                        {"whitespace": " "},
                        {"keyword": "join"},
                        {"whitespace": " "},
                        {
                            "from_expression_element": {
                                "table_expression": {
                                    "table_reference": {"identifier": "d"}
                                }
                            }
                        },
                    ],
                    "whitespace": " ",
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "from_expression": {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "e"}}
                    }
                }
            },
        ],
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_from_three_table():
    parsed = sqlfluff.parse(
        """select * from a join b join c""",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "from_clause": {
            "from_expression": [
                {
                    "from_expression_element": {
                        "table_expression": {"table_reference": {"identifier": "a"}}
                    }
                },
                {"whitespace": " "},
                {
                    "join_clause": {
                        "from_expression_element": {
                            "table_expression": {"table_reference": {"identifier": "b"}}
                        },
                        "keyword": "join",
                        "whitespace": " ",
                    }
                },
                {"whitespace": " "},
                {
                    "join_clause": {
                        "from_expression_element": {
                            "table_expression": {"table_reference": {"identifier": "c"}}
                        },
                        "keyword": "join",
                        "whitespace": " ",
                    }
                },
            ],
            "keyword": "from",
            "whitespace": " ",
        },
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "wildcard_expression": {"wildcard_identifier": {"star": "*"}}
            },
            "whitespace": " ",
        },
        "whitespace": " ",
    }


def test_expressions():
    parsed = sqlfluff.parse(
        """select 1 + 2, not (a = b), x <> y""",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "select_clause": [
            {"keyword": "select"},
            {"whitespace": " "},
            {
                "select_clause_element": {
                    "expression": [
                        {"literal": "1"},
                        {"whitespace": " "},
                        {"binary_operator": "+"},
                        {"whitespace": " "},
                        {"literal": "2"},
                    ]
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "select_clause_element": {
                    "function": {
                        "bracketed": {
                            "end_bracket": ")",
                            "expression": [
                                {"column_reference": {"identifier": "a"}},
                                {"whitespace": " "},
                                {
                                    "comparison_operator": {
                                        "raw_comparison_operator": "="
                                    }
                                },
                                {"whitespace": " "},
                                {"column_reference": {"identifier": "b"}},
                            ],
                            "start_bracket": "(",
                        },
                        "function_name": {"function_name_identifier": "not"},
                        "whitespace": " ",
                    }
                }
            },
            {"comma": ","},
            {"whitespace": " "},
            {
                "select_clause_element": {
                    "expression": [
                        {"column_reference": {"identifier": "x"}},
                        {"whitespace": " "},
                        {
                            "comparison_operator": [
                                {"raw_comparison_operator": "<"},
                                {"raw_comparison_operator": ">"},
                            ]
                        },
                        {"whitespace": " "},
                        {"column_reference": {"identifier": "y"}},
                    ]
                }
            },
        ],
    }


def test_simple_arithmetic_expressions():
    parsed = sqlfluff.parse(
        """select 1 + 2""",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "expression": [
                    {"literal": "1"},
                    {"whitespace": " "},
                    {"binary_operator": "+"},
                    {"whitespace": " "},
                    {"literal": "2"},
                ]
            },
            "whitespace": " ",
        }
    }


def test_arithmetic_expressions_with_function_call():
    parsed = sqlfluff.parse(
        """select 1 + sum(a + b)""",
    )
    assert parsed["file"]["statement"]["select_statement"] == {
        "select_clause": {
            "keyword": "select",
            "select_clause_element": {
                "expression": [
                    {"literal": "1"},
                    {"whitespace": " "},
                    {"binary_operator": "+"},
                    {"whitespace": " "},
                    {
                        "function": {
                            "bracketed": {
                                "end_bracket": ")",
                                "expression": [
                                    {"column_reference": {"identifier": "a"}},
                                    {"whitespace": " "},
                                    {"binary_operator": "+"},
                                    {"whitespace": " "},
                                    {"column_reference": {"identifier": "b"}},
                                ],
                                "start_bracket": "(",
                            },
                            "function_name": {"function_name_identifier": "sum"},
                        }
                    },
                ]
            },
            "whitespace": " ",
        }
    }
