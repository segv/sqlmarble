from sqlmarble.nodes import (
    Clause,
    CommaList,
    File,
    FoldableLines,
    Identifier,
    Sequence,
    Text,
)
from sqlmarble.parser import parse_sql_string
from sqlmarble.dialects import lookup_dialect


def parse(sql, dialect=None):
    return parse_sql_string(sql, dialect=lookup_dialect(dialect))


def test_select_1():
    assert parse("Select 1") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text("select"),
                        clause=CommaList(children=[Text(text="1")]),
                    )
                ]
            )
        ]
    )


def test_select_1_trailing_whitespace():
    assert parse("Select  1   ") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text("select"),
                        clause=CommaList(children=[Text(text="1")]),
                    )
                ]
            ),
        ]
    )


def test_select_1_2():
    assert parse("Select  1111,   2222") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text("select"),
                        clause=CommaList(
                            children=[Text(text="1111"), Text(text="2222")]
                        ),
                    )
                ]
            )
        ]
    )


def test_select_with_from():
    assert parse("select 1, 2 from x") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text("select"),
                        clause=CommaList(children=[Text(text="1"), Text(text="2")]),
                    ),
                    Clause(
                        label=Text("from"),
                        clause=Identifier(identifier="x"),
                    ),
                ]
            )
        ]
    )


def test_select_with_comma_froms():
    assert parse("select * from a, b") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text(text="select"),
                        clause=CommaList(
                            children=[Text(text="*")],
                        ),
                    ),
                    Clause(
                        label=Text(text="from"),
                        clause=CommaList(
                            children=[
                                Identifier(identifier="a"),
                                Identifier(identifier="b"),
                            ],
                        ),
                    ),
                ],
            )
        ],
    )


def test_select_with_from_joins():
    assert parse("SELECT * FROM Aaaaa INNER JOIN B USING (id)") == File(
        children=[
            FoldableLines(
                children=[
                    Clause(
                        label=Text(text="select"),
                        clause=CommaList(children=[Text(text="*")]),
                    ),
                    FoldableLines(
                        children=[
                            Clause(
                                label=Text(text="from"),
                                clause=Identifier(identifier="Aaaaa"),
                            ),
                            Clause(
                                label=Text(text="inner join"),
                                clause=FoldableLines(
                                    children=[
                                        Identifier(identifier="B"),
                                        Sequence(
                                            children=[
                                                Text(text="using"),
                                                Text(text=" "),
                                                Text(text="(id)"),
                                            ]
                                        ),
                                    ]
                                ),
                            ),
                        ]
                    ),
                ]
            )
        ]
    )
