# sqlmarble - a zero-configuration code formatter for sql 

## Why?

Because I don't want to discuss sql formatting, but I do want all the
code produced by the entire team to look the same.
