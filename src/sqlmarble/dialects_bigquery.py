import re

from sqlmarble.dialects import Dialect

BIGQUERY_RESERVED_KEYWORDS = """
    ALL AND ANY ARRAY AS ASC ASSERT_ROWS_MODIFIED AT
    BETWEEN BY
    CASE CAST COLLATE CONTAINS CREATE CROSS CUBE CURRENT
    DEFAULT DEFINE DESC DISTINCT
    ELSE END ENUM ESCAPE EXCEPT EXCLUDE EXISTS EXTRACT
    FALSE FETCH FOLLOWING FOR FROM FULL
    GROUP GROUPING GROUPS
    HASH HAVING
    IF IGNORE IN INNER INTERSECT INTERVAL INTO IS
    JOIN
    LATERAL LEFT LIKE LIMIT LOOKUP
    MERGE
    NATURAL NEW NO NOT NULL NULLS
    OF ON OR ORDER OUTER OVER
    PARTITION PRECEDING PROTO
    QUALIFY
    RANGE RECURSIVE RESPECT RIGHT ROLLUP ROWS
    SELECT SET SOME STRUCT
    TABLESAMPLE THEN TO TREAT TRUE
    UNBOUNDED UNION UNNEST USING
    WHEN WHERE WINDOW WITH WITHIN
"""

BIGQUERY_RESERVED_KEYWORDS = BIGQUERY_RESERVED_KEYWORDS.split()
BIGQUERY_RESERVED_KEYWORDS = [s.strip() for s in BIGQUERY_RESERVED_KEYWORDS]
BIGQUERY_RESERVED_KEYWORDS = [s for s in BIGQUERY_RESERVED_KEYWORDS if s != ""]

# NOTE TABLE is not in the list of reserved keywords at
# https://cloud.google.com/bigquery/docs/reference/standard-sql/lexical#reserved_keywords
# .  let's assume that's a (and not that there's something weirder
# going on) 20220619:mb

BIGQUERY_RESERVED_KEYWORDS.append("TABLE")


class BigQuery(Dialect):
    sqlfluff_name = "bigquery"

    def normalize_identifier(self, identifier):
        if identifier.startswith("`") and identifier.endswith("`"):
            ref = identifier[1:-1]
        else:
            ref = identifier.lower()
            if identifier.upper() in BIGQUERY_RESERVED_KEYWORDS:
                return "`" + ref + "`"
            else:
                return ref

        normalized_parts = []

        for part in ref.split("."):
            all_lowercase_alphanum = re.match("^[a-z_][a-z0-9_]*$", part)
            is_reserved_keyword = part.upper() in BIGQUERY_RESERVED_KEYWORDS
            if all_lowercase_alphanum and not is_reserved_keyword:
                normalized_parts.append(part)
            else:
                normalized_parts.append("`" + part + "`")

        return ".".join(normalized_parts)
