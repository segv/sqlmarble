import apm


def arr_join(array, separator):
    if len(array) == 0:
        return []
    elif len(array) == 1:
        return array
    else:
        arr = [array[0]]
        for a in array[1:]:
            arr.extend(separator)
            arr.append(a)
        return arr


class Matcher:
    @classmethod
    def match(cls, *args, **kwargs):
        return apm.match(*args, **kwargs)

    def __init__(self, value, strict=True, multimatch=False):
        self.value = value
        self.result = None
        self.strict = strict
        self.multimatch = multimatch

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        return None

    def __call__(self, pattern, multimatch=None):
        if self.strict:
            pattern = apm.Strict(pattern)
        if multimatch is None:
            multimatch = self.multimatch
        self.result = apm.match(self.value, pattern, multimatch=multimatch)
        return self.result

    def __getitem__(self, key):
        if self.result is None:
            raise KeyError
        else:
            try:
                return self.result[key]
            except KeyError:
                return None

    IDENTIFIER = {"identifier": "identifier" @ apm.InstanceOf(str)}


def Optional(x):
    return x | apm._
