from dataclasses import dataclass, field
from typing import List


@dataclass
class LayoutNode:
    fits_on_line: bool = field(init=False, default=False)

    def __repr__(self):
        return f"{self.__class__.__name__}()"


def split_children(children):
    new_children = []
    for i, child in enumerate(children):
        if child.fits_on_line:
            continue

        new_child, child_did_split = child.split()
        if child_did_split:
            new_children = children[0:i] + [new_child] + children[i + 1 :]
            return new_children, True

    return new_children, False


@dataclass
class Container(LayoutNode):
    children: List[LayoutNode] = field(default_factory=list)


@dataclass
class Text(LayoutNode):
    text: str

    def split(self):
        return self, False


@dataclass
class Identifier(LayoutNode):
    identifier: str

    def split(self):
        return self, False


@dataclass
class Newline(LayoutNode):
    pass

    def split(self):
        return self, False


@dataclass
class File(Container):
    def split(self):
        new_children, split = split_children(self.children)
        if new_children:
            return File(new_children), True
        else:
            return self, False


@dataclass
class FoldableLines(Container):
    def split(self):
        return Lines(self.children), True


@dataclass
class Lines(Container):
    def split(self):
        new_children, split = split_children(self.children)
        if split:
            return Lines(new_children), True
        else:
            return self, False


@dataclass
class Indent(LayoutNode):
    body: LayoutNode

    def split(self):
        new_body, did_split = self.body.split()
        if did_split:
            return Indent(new_body), True
        else:
            return self, False


@dataclass
class Clause(LayoutNode):
    """
    A "clause" in an sql statement. Will render as either:

        <label> <clause>

    or

        <label>
          <clause>
    """

    label: LayoutNode
    clause: LayoutNode

    def split(self):
        seq = Sequence()
        seq.children.append(self.label)
        seq.children.append(Indent(Sequence([Newline(), self.clause])))
        return seq, True


@dataclass
class CommaList(Container):
    """
    A Comma separated list of equally import things. Used for the clauses in a select or the conditions in an order by. Will render as either:

        a, b, c, ...

    or:

        a
        ,b
        ,c
    """

    def split(self):
        if len(self.children) == 0:
            return self, False
        elif len(self.children) == 1:
            return self.children[0], True
        else:
            seq = Sequence()
            seq.children.append(self.children[0])
            for child in self.children[1:]:
                seq.children.extend([Newline(), Text(","), child])
            return seq, True


@dataclass
class ArgumentList(Container):
    """
    A Comma separated list of arguments to a function, with block delimiters. Will render as either:

        name(a, b, c, ...)

    or:

        name(a
             b,
             c
        )

     or:

        name(
          a
          b,
          c
        )

    """


@dataclass
class Sequence(Container):
    def split(self):
        new_children, split = split_children(self.children)
        if split:
            return Sequence(new_children), True
        else:
            return self, False
