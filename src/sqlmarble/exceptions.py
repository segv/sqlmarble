from pprint import pformat, pprint  # noqa: F401


class SQLMarbleException(Exception):
    pass


class ParseFailure(SQLMarbleException):
    def __init__(self, ast, context=None, message="Unable to parse ast"):
        self.ast = ast
        self.context = context

        self.message = message + " " + pformat(ast)
        if context is not None:
            self.message += f" (nested inside {pformat(context)})"
        super().__init__(self.message)


class FormatterFailure(SQLMarbleException):
    pass
