import re

from sqlmarble.dialects import Dialect


class ANSI(Dialect):
    sqlfluff_name = "ansi"

    def normalize_identifier(self, identifier):
        if re.match(r"^[a-zA-Z_][a-zA-Z_0-9]*$", identifier):
            return identifier.lower()
        else:
            return identifier
