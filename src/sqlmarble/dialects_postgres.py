import re

from sqlmarble.dialects import Dialect

# NOTE See https://docs.google.com/spreadsheets/d/1OFgkuHKIEeqAqvMyvaVV8nZQwj_LDUU5E2y-IfDyKzw/edit#gid=1147914610 for how this list is generated. 20220619:mb


POSTGRESQL_14_RESERVED_KEYWORDS = """
    ALL ANALYSE ANALYZE AND ANY ARRAY AS ASC ASYMMETRIC
    BOTH
    CASE CAST CHECK COLLATE COLUMN CONSTRAINT CREATE CURRENT_CATALOG CURRENT_DATE CURRENT_ROLE CURRENT_TIME CURRENT_TIMESTAMP CURRENT_USER
    DEFAULT DEFERRABLE DESC DISTINCT DO
    ELSE END EXCEPT
    FALSE FETCH FOR FOREIGN FROM
    GRANT GROUP
    HAVING
    IN INITIALLY INTERSECT INTO
    LATERAL LEADING LIMIT LOCALTIME LOCALTIMESTAMP
    NOT NULL
    OFFSET ON ONLY OR ORDER
    PLACING PRIMARY
    REFERENCES RETURNING
    SELECT SESSION_USER SOME SYMMETRIC
    TABLE THEN TO TRAILING TRUE
    UNION UNIQUE USER USING
    VARIADIC
    WHEN WHERE WINDOW WITH
"""


POSTGRESQL_14_RESERVED_KEYWORDS = POSTGRESQL_14_RESERVED_KEYWORDS.lower()
POSTGRESQL_14_RESERVED_KEYWORDS = POSTGRESQL_14_RESERVED_KEYWORDS.split()
POSTGRESQL_14_RESERVED_KEYWORDS = [s.strip() for s in POSTGRESQL_14_RESERVED_KEYWORDS]
POSTGRESQL_14_RESERVED_KEYWORDS = [
    s for s in POSTGRESQL_14_RESERVED_KEYWORDS if s != ""
]


class Postgres(Dialect):
    sqlfluff_name = "postgres"

    def normalize_identifier(self, identifier):
        if identifier.startswith('"') and identifier.endswith('"'):
            ref = identifier[1:-1]
            all_lowercase_alphanum = re.match("^[a-z_][a-z0-9_$]*$", ref)
            is_reserved_keyword = ref.lower() in POSTGRESQL_14_RESERVED_KEYWORDS
            if all_lowercase_alphanum and not is_reserved_keyword:
                return ref
            else:
                return identifier
        else:
            ref = identifier.lower()
            if ref in POSTGRESQL_14_RESERVED_KEYWORDS:
                return '"' + ref + '"'
            else:
                return ref
