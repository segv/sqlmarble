import logging
from copy import copy, deepcopy
from dataclasses import dataclass
from functools import reduce
from pprint import pformat, pprint  # noqa: F401

import sqlmarble.nodes as n
from sqlmarble.dialects import Dialect, lookup_dialect
from sqlmarble.exceptions import FormatterFailure
from sqlmarble.parser import parse_sql_string
from sqlmarble.utils import arr_join

log = logging.getLogger(__name__)


def format_sql_string(sql, dialect=None, max_line_length=None):
    dialect = lookup_dialect(dialect)
    return render_to_chunks(
        parse_sql_string(sql, dialect=dialect),
        dialect=dialect,
        max_line_length=max_line_length,
    )


class NodeVisitor:
    def __init__(self):
        self.depth = 0

    def debug(self, msg):
        log.debug(" " * self.depth + msg)

    def get_visitor(self, node):
        name = f"visit_{node.__class__.__name__}"
        visitor = getattr(self, name, None)
        if visitor is None:
            raise FormatterFailure(f"No known {self.__class__.__name__} for {node}")

        def v(node, *args, **kwargs):
            if log.isEnabledFor(logging.DEBUG):
                self.depth += 2
                args_str = [pformat(a) for a in [node] + list(args)]
                kwargs_str = [k + "=" + pformat(v) for k, v in kwargs.items()]
                self.debug("visit(" + ",".join(args_str + kwargs_str) + ")")

                ret = visitor(node, *args, **kwargs)
                self.debug("=> " + pformat(ret))
                self.depth -= 2

                return ret
            else:
                return visitor(node, *args, **kwargs)

        return v

    def visit(self, node, *args, **kwargs):
        return self.get_visitor(node)(node, *args, **kwargs)


def render_to_chunks(node, dialect=None, max_line_length=None):
    while True:
        renderer = Renderer(dialect=dialect, max_line_length=max_line_length)
        state = renderer.visit(node, RenderState())
        if state.max_extent() <= renderer.max_line_length:
            return "".join(state.chunks)

        node, did_split = node.split()

        if not did_split:
            return "".join(state.chunks)


class RenderState:
    def __init__(self, chunks=[], indent_level=None):
        if chunks is None:
            chunks = list()
        self.chunks = chunks

        if indent_level is None:
            indent_level = 0
        self.indent_level = indent_level

    def copy(self, chunks=None, indent_level=None):
        if chunks is None:
            chunks = deepcopy(self.chunks)
        if indent_level is None:
            indent_level = self.indent_level
        return RenderState(chunks=chunks, indent_level=indent_level)

    def collect(self, chunk):
        return self.copy(chunks=self.chunks + [chunk])

    def collect_indent(self):
        if self.indent_level > 0:
            return self.collect(" " * self.indent_level)
        else:
            return self

    def newline(self):
        return self.collect("\n").collect_indent()

    def indent(self, delta):
        return self.copy(indent_level=self.indent_level + delta)

    def max_extent(self):
        # this is very slow. let's optimise it soon.
        text = "".join(self.chunks)
        lines = text.split("\n")
        lengths = (len(line) for line in lines)
        return reduce(max, lengths, 0)

    def __repr__(self):
        doc = "".join(self.chunks)
        doc = doc.replace("\n", "\\n")
        return self.__class__.__name__ + "(" + doc + ")"


@dataclass
class Renderer(NodeVisitor):
    max_line_length: int = 88
    indent_step: int = 2
    dialect: Dialect = None

    def __post_init__(self):
        super().__init__()
        if self.max_line_length is None:
            self.max_line_length = 88

    def visit_File(self, node, s):
        for child in node.children:
            s = self.visit(child, s)
            s = s.collect("\n")
        return s

    def visit_Text(self, node, s):
        return s.collect(node.text)

    def visit_Identifier(self, node, s):
        return s.collect(self.dialect.normalize_identifier(node.identifier))

    def visit_Clause(self, node, s):
        s = self.visit(node.label, s)
        s = self.visit(n.Text(" "), s)
        s = self.visit(node.clause, s)
        return s

    def visit_CommaList(self, node, s):
        return self.visit(n.Sequence(arr_join(node.children, [n.Text(", ")])), s)

    def visit_FoldableLines(self, node, s):
        return self.visit(n.Sequence(arr_join(node.children, [n.Text(" ")])), s)

    def visit_Lines(self, node, s):
        if len(node.children) == 0:
            return s
        else:
            children = copy(node.children)

            while True:
                child = children.pop(0)
                s = self.visit(child, s)
                if children:
                    if s.max_extent() <= self.max_line_length:
                        child.fits_on_line = True
                    s = s.newline()
                else:
                    break

            return s

    def visit_Indent(self, node, s):
        s = s.indent(+self.indent_step)
        s = self.visit(node.body, s)
        s = s.indent(-self.indent_step)
        return s

    def visit_Sequence(self, node, s):
        for child in node.children:
            s = self.visit(child, s)
        return s

    def visit_Newline(self, node, s):
        return s.newline()
