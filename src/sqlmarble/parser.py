from copy import copy, deepcopy
from pprint import pformat, pprint  # noqa: F401

import sqlfluff
from apm import InstanceOf, Items, OneOf, Remainder, Remaining, Strict, _

import sqlmarble.nodes as n
from sqlmarble.exceptions import ParseFailure
from sqlmarble.utils import Matcher, Optional, arr_join


def parse_sql_string(sql, dialect=None):
    return walk_ast(sqlfluff.parse(sql, dialect=dialect.sqlfluff_name))


class ParseFailure(Exception):
    def __init__(self, ast, context=None, message="Unable to parse ast"):
        self.ast = ast
        self.context = context

        self.message = message + " " + pformat(ast)
        if context is not None:
            self.message += f" (nested inside {pformat(context)})"
        super().__init__(self.message)


def walk_ast(ast):
    with Matcher(ast) as m:
        if m({"file": "result" @ _}):
            return walk_file(m["result"])
        if m({"statement": "result" @ _}):
            return walk_statement(m["result"])
        if m({"comment": "result" @ _}):
            return walk_comment(m["result"])

    raise ParseFailure(ast, message="Unknown ast type")


def walk_file(ast):
    ast_for_error = deepcopy(ast)
    ast = without_whitespace(ast)

    nodes = []
    if isinstance(ast, dict):
        if "statement" in ast:
            nodes.append(walk_statement(ast.pop("statement")))

        if ast:
            raise ParseFailure(
                ast,
                context=ast_for_error,
                message="extra unrecognized elements in ast",
            )

    else:
        nodes = [walk_ast(a) for a in ast]

    return n.File(children=nodes)


def walk_whitespace(ast):
    return n.Text(ast)


def walk_newline(ast):
    return n.Newline()


def walk_comment(ast):
    if ast.startswith("--"):
        return n.Lines([n.Text(ast)])
    else:
        raise ParseFailure(ast)


def walk_statement(ast):
    with Matcher(ast) as matcher:
        if matcher({"select_statement": "result" @ _}):
            return walk_select_statement(matcher["result"])
    raise ParseFailure(ast)


def walk_select_statement(ast):
    ast_for_error = deepcopy(ast)

    children = []
    select_clause = None
    from_clause = None

    if isinstance(ast, dict):
        with Matcher(ast) as matcher:
            if matcher(
                Items(select_clause="select_clause" @ _)
                & Optional(Items(from_clause="from_clause" @ _))
            ):
                select_clause = matcher["select_clause"]
                from_clause = matcher["from_clause"]
            else:
                raise ParseFailure(ast)
    else:
        for clause in ast:
            with Matcher(clause) as matcher:
                if matcher(Items(select_clause="result" @ _)):
                    select_clause = matcher["result"]
                elif matcher(Items(from_clause="result" @ _)):
                    from_clause = matcher["result"]
                else:
                    raise ParseFailure(clause, ast_for_error)

    children.append(walk_select_clause(select_clause, ast_for_error))

    if from_clause is not None:
        children.append(walk_from_clause(from_clause, ast_for_error))

    return n.FoldableLines(children)


def walk_select_clause(select_clause, ast_for_error):
    if isinstance(select_clause, dict):
        with Matcher(select_clause) as matcher:
            if matcher(Items(keyword=_, select_clause_element="result" @ _)):
                select_clause = [
                    {"keyword": "select"},
                    {"select_clause_element": matcher["result"]},
                ]
            else:
                raise ParseFailure(select_clause, ast_for_error)

    with Matcher(select_clause) as matcher:
        if matcher([Items(keyword=_), "result" @ Remaining(...)]):
            select_clause = matcher["result"]
        else:
            raise ParseFailure(select_clause[0], ast_for_error)

    select_nodes = []
    for segment in select_clause:
        if Matcher.match(
            segment,
            {"select_clause_element": "select_clause_element" @ _},
        ):
            select_nodes.append(
                walk_select_clause_element(segment.pop("select_clause_element"))
            )
        elif not Matcher.match(
            segment,
            OneOf({"whitespace": _}, {"comma": _}, {"newline": _}),
        ):
            raise ParseFailure(segment, ast_for_error)

    return n.Clause(label=n.Text("select"), clause=n.CommaList(children=select_nodes))


def walk_select_clause_element(ast):
    ast_for_error = copy(ast)

    alias = None
    value = None

    with Matcher(ast, strict=False) as matcher:
        if matcher(Items(alias_expression=Matcher.IDENTIFIER)):
            ast.pop("alias_expression")
            alias = walk_identifier(matcher["identifier"])

    with Matcher(ast) as matcher:
        if matcher(Items(literal="result" @ InstanceOf(str))):
            value = walk_literal(matcher["result"])

        if matcher(Items(column_reference="result" @ _)):
            value = walk_column_reference(matcher["result"])

        if matcher(Items(wildcard_expression="result" @ _)):
            value = walk_wildcard_expression(matcher["result"])

        if matcher(Items(expression="result" @ _)):
            value = walk_expression(matcher["result"])

    if value is None:
        raise ParseFailure(ast, ast_for_error)

    if alias:
        return n.FoldableLines([value, n.FoldableLines([n.Text("as"), alias])])
    else:
        return value


def walk_literal(ast):
    if ast.lower() in "true false null".split():
        ast = ast.lower()

    return n.Text(ast)


def walk_wildcard_expression(ast):
    with Matcher(ast) as matcher:
        if matcher({"wildcard_identifier": Strict({"star": "*"})}):
            return n.Text("*")
    raise ParseFailure(ast)


def walk_column_reference(ast):
    with Matcher(ast) as matcher:
        if matcher(Matcher.IDENTIFIER):
            return walk_identifier(matcher["identifier"])

    parts = []
    for p in ast:
        with Matcher(p) as matcher:
            if matcher({"dot": _}):
                parts.append(n.Text("."))
            elif matcher(Matcher.IDENTIFIER):
                parts.append(walk_identifier(matcher["identifier"]))
            else:
                raise ParseFailure(ast)
    return n.Sequence(parts)


def walk_identifier(ast):
    return n.Identifier(ast)


def walk_from_clause(from_clause, ast_for_error):
    if isinstance(from_clause, dict):
        from_clause_for_error = deepcopy(from_clause)
        as_list = [
            {"keyword": from_clause.pop("keyword")},
            {"from_expression": from_clause.pop("from_expression")},
        ]
        if len(from_clause) > 0:
            raise ParseFailure(from_clause_for_error, ast_for_error)
        from_clause = as_list

    if "keyword" in from_clause[0] and len(from_clause[0]) == 1:
        from_clause.pop(0)["keyword"]
    else:
        raise ParseFailure(from_clause[0], ast_for_error)

    have_commas = False
    for segment in from_clause:
        if len(segment) == 1:
            if "comma" in segment:
                have_commas = True

    if have_commas:
        comma_list = []
        for segment in from_clause:
            if len(segment) == 1:
                if "from_expression" in segment:
                    walked = walk_from_expression(segment.pop("from_expression"), False)
                    comma_list.append(walked)

                if "comma" in segment:
                    segment.pop("comma", None)

                if segment:
                    raise ParseFailure(segment, ast_for_error)
            else:
                raise ParseFailure(segment, ast_for_error)

        return n.Clause(label=n.Text("from"), clause=n.CommaList(comma_list))
    else:
        from_expression = None
        for segment in from_clause:
            if len(segment) == 1:
                if "from_expression" in segment:
                    if from_expression is not None:
                        raise ParseFailure(segment, ast_for_error)
                    from_expression = walk_from_expression(
                        segment.pop("from_expression"), True
                    )

                if segment:
                    raise ParseFailure(segment, ast_for_error)
            else:
                raise ParseFailure(segment, ast_for_error)

        return from_expression


def walk_from_expression(ast, inject_from_keyword):
    if isinstance(ast, dict):
        from_expression = [{"from_expression_element": ast["from_expression_element"]}]
        if "join_clause" in ast:
            from_expression.append({"join_clause": ast["join_clause"]})

    parts = []

    for element in from_expression:
        if len(element) == 1:
            if "from_expression_element" in element:
                parts.append(
                    walk_from_expression_element(
                        element["from_expression_element"],
                        inject_from_keyword and len(parts) == 0,
                    )
                )
                continue
            if "join_clause" in element:
                parts.append(walk_join_clause(element["join_clause"]))
                continue

        raise ParseFailure(element)

    if len(parts) == 1:
        return parts[0]
    else:
        return n.FoldableLines(parts)


def walk_from_expression_element(ast, inject_from_keyword):
    ast_for_error = deepcopy(ast)

    table = None
    if "table_expression" in ast:
        table_reference = ast["table_expression"].get("table_reference", None)
        if table_reference is not None:
            table = walk_table_reference(table_reference)

    if table is None:
        raise ParseFailure(ast)

    alias_expression = ast.get("alias_expression", None)
    if alias_expression:
        alias = walk_identifier(alias_expression.pop("identifier"))

        if alias_expression:
            raise ParseFailure(ast, ast_for_error)

    if alias_expression:
        # nest the foldable lines this way so we expand in this order: table as alias ; table\nas alias ; table\nas\nalias
        expr = n.FoldableLines(table, n.FoldableLines(n.Text("as"), alias))
    else:
        expr = table

    if inject_from_keyword:
        return n.Clause(label=n.Text("from"), clause=expr)
    else:
        return expr


def walk_join_clause(ast):
    keywords = []
    from_ = None
    join_condition = []

    for element in ast:
        with Matcher(element) as matcher:
            if matcher({"from_expression_element": "result" @ _}):
                if from_ is None:
                    from_ = walk_from_expression_element(matcher["result"], False)
                    continue
                else:
                    raise ParseFailure(element, ast)
            elif matcher({"keyword": "result" @ InstanceOf(str)}):
                if from_ is None:
                    keywords.append(matcher["result"].lower())
                else:
                    join_condition.append(n.Text(matcher["result"].lower()))
            elif matcher(
                Items(
                    bracketed=Items(
                        start_bracket="start_bracket" @ InstanceOf(str),
                        identifier="identifier" @ InstanceOf(str),
                        end_bracket="end_bracket" @ InstanceOf(str),
                    )
                )
            ):
                join_condition.append(
                    n.Text(
                        matcher["start_bracket"]
                        + matcher["identifier"]
                        + matcher["end_bracket"]
                    )
                )
            else:
                raise ParseFailure(ast)

    if len(join_condition) > 0:
        clause = n.FoldableLines(
            [from_, n.Sequence(arr_join(join_condition, [n.Text(" ")]))]
        )
    else:
        clause = from_

    return n.Clause(
        label=n.Text(" ".join(keywords)),
        clause=clause,
    )


def walk_table_reference(ast):
    table_reference = ast

    with Matcher(table_reference) as matcher:
        if matcher(Matcher.IDENTIFIER):
            table_reference = [{"identifier": matcher["identifier"]}]

    parts = []
    for part in table_reference:
        with Matcher(part) as matcher:
            if matcher(Items(dot=".")):
                continue
            elif matcher(Matcher.IDENTIFIER):
                parts.append(walk_identifier(part["identifier"]))
            else:
                raise ParseFailure(part, ast)
    if len(parts) == 1:
        return parts[0]
    else:
        return n.Sequence(arr_join(parts, [n.Text(".")]))


def walk_keyword(ast):
    return n.Text(ast.lower())


def walk_binary_operator(ast):
    return n.Text(ast)


def walk_comparison_operator(ast):
    with Matcher(ast, strict=False) as matcher:
        if matcher({"raw_comparison_operator": "result" @ InstanceOf(str)}):
            return n.Text(matcher["result"])
    raise ParseFailure(ast)


def walk_expression(ast):
    if isinstance(ast, dict):
        with Matcher(ast, strict=False) as matcher:
            if matcher(
                {"keyword": "keyword" @ InstanceOf(str)} ** Remainder("rest" @ _)
            ):
                as_list = [{"keyword": matcher["keyword"]}]
                for k, v in matcher["rest"].items():
                    as_list.append({k: v})
                ast = as_list
            else:
                raise ParseFailure(ast)

    lines = None
    for node in ast:
        with Matcher(node) as matcher:
            element = None
            new_line = None
            if matcher({"literal": "result" @ InstanceOf(str)}):
                element = walk_literal(matcher["result"])
            elif matcher({"column_reference": "result" @ _}):
                element = walk_column_reference(matcher["result"])
            elif matcher({"binary_operator": "result" @ InstanceOf(str)}):
                new_line = walk_binary_operator(matcher["result"])
            elif matcher({"comparison_operator": "result" @ _}):
                new_line = walk_comparison_operator(matcher["result"])
            elif matcher({"keyword": "result" @ InstanceOf(str)}):
                new_line = walk_keyword(matcher["result"])

            if element is not None:
                if lines is None:
                    lines = [[element]]
                else:
                    lines[-1].append(element)
            elif new_line is not None:
                if lines is None:
                    lines = [[new_line]]
                else:
                    lines.append([new_line])
            else:
                raise ParseFailure(node, ast)

    return n.FoldableLines([n.FoldableLines(line) for line in lines])


def without_whitespace(ast):
    if isinstance(ast, str):
        return ast
    elif isinstance(ast, dict):
        new = {}
        for k, v in ast.items():
            if k not in ("whitespace", "newline"):
                new[k] = without_whitespace(v)
        return new
    else:
        new = []
        for segment in ast:
            if not Matcher.match(segment, OneOf({"whitespace": _}, {"newline": _})):
                new.append(without_whitespace(segment))
        return new
