def lookup_dialect(dialect):
    if dialect == "bigquery":
        from sqlmarble.dialects_bigquery import BigQuery

        return BigQuery()
    elif dialect == "postgres":
        from sqlmarble.dialects_postgres import Postgres

        return Postgres()
    elif dialect is None or dialect == "ansi":
        from sqlmarble.dialects_ansi import ANSI

        return ANSI()
    else:
        raise ValueError(f"Unknown dialect {dialect}")


class Dialect:
    def normalize_identifier(self, identifier):
        return identifier
