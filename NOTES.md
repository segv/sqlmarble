# NOTES

Do we ever actually have the case where a decision on line N changes
the layout on some line after N? What's the down side of our current
idea of just starting with everyong on a single line an diterativelly
splitting until we're happy?

## A bunch of examples

### 1

```
01234567890123456789
select 1, 2, 3, 4, 5
```

with a target line length of 12

```
01234567890123456789
select
  1
  ,2
  ,3
  ,4
  ,5
```

### 2


```
01234567890123456789
select sum(a + b + c)
```

with a target line length of 12

```
01234567890123456789
select 
  sum(
    a + b + c
  )
```

with a target line length of 4:

```
01234567890123456789
select
  sum(
    a
    + b
    + c
  )
```

note that in this case we're not allowed to do:

```
01234567890123456789
select sum(a
           + b
           + c
  )
```

so if we can't fit a + b + c on one line we need to split the select.

our "algorithm" is super simple, put it all on one line and iteratively split it until we fit in the alloted width:

```
select sum(a + b + c)
```

```
select
  sum(a + b + c)
```

```
select
  sum(a 
      + b 
      + c)
```

```
select
  sum(
    a 
    + b 
    + c
  )
```


```
select case when a = 1 then b else null end as foo
```

```
select 
  case when a = 1 then b else null end as foo
```

```
select 
  case 
    when a = 1 then b 
    else null 
  end as foo
```

```
select 
  case 
    when 
      a = 1 
    then 
      b 
    else null 
  end 
  as foo
```

```
with foo as (select 1 from bar)
select * from foo
```

```
with
  foo as (select 1 from bar)
select * from foo
```

```
with
  foo as (
    select 1 from bar
  )
select * from foo
```

```
with
  foo as (
    select 
      1 
    from bar
  )
select * from foo
```

```
with
  foo as (
    select 
      1 
    from 
      bar
  )
select * from foo
```


```
with a as (with b as (select 1 from c)
           select * from b)
select * from foo
```

## kinds of sql

### text

unbreakable sequence of charaetrs

### multi line

can be one line or multiple lines at the same indentation

```
select 1
from foo
where bar
group by boo
```

### labeled line

has a keyword at the start can break after the keyword

```
select 123456789
```

```
select
  1234567890
```

```
with ...
```

```
with
  ...
```

### comma list

```
a, b, c, d
```

```
a
,b
,c
,d
```

### nested

```
(...)
```

```
(
  ...
)
```
